let app = require('../server/server');

const dataSource = app.dataSources.localdb;
//
dataSource.automigrate((err) => {
  if (err) {
    console.log(err.message);
  }

  console.log('Database migration was successfull.');
  process.exit(0);
});

