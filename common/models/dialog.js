const ObjectID = require('bson-objectid');
const uuidv4 = require('uuid/v4');
const { PassThrough } = require('stream');

const { getApp } = require('../helpers/');
const dispatcher = require('../helpers/dispatcher');

module.exports = async (Dialog) => {
  const app = await getApp(Dialog);

  Dialog.afterRemote('find', async (ctx) => {
    const { filter } = ctx.args;
    if ('members' in filter && filter.members === 'include') {
      const promises = ctx.result.map(async item => {
        const users = await app.models.User.find({ id: item.members });
        item.members = users;
        return item;
      });
      await Promise.all(promises);
    }
  });

  Dialog.message = (id, text, options, cb) => {
    Dialog.getDataSource().connector.connect((err, db) => {
      const message = getMessage(text, options.accessToken.userId);
      db.collection('dialog').update(
        { _id: ObjectID(id) },
        { $push: { messages: message } },
        (err, res) => {
          if (err) return cb(err);
          dispatcher.emit(`dialog/message::${id}`, message);
          setTimeout(
            () => dispatcher.emit(
              `dialog/message::${id}`,
              getMessage(uuidv4())
            ),
            3000
          );
          return cb(null, message);
        });
    });
    function getMessage(text, author = 'fake') {
      return {
        _id: ObjectID.generate(),
        text,
        author,
        created: new Date(),
      };
    }
  };
  Dialog.remoteMethod('message', {
    accepts: [{
      arg: 'id', type: 'string',
    }, {
      arg: 'text', type: 'string',
    }, {
      arg: 'options', type: 'object', http: 'optionsFromRequest',
    }],
    returns: { type: 'object', root: true },
    http: { path: '/:id/message', verb: 'post' },
    description: 'add message to dialog',
  });

  Dialog['message-stream'] = (id, next) => {
    let changes = new PassThrough({ objectMode: true });
    let writeable = true;

    let time = 0;
    changes.destroy = () => {
      changes.removeAllListeners('error');
      changes.removeAllListeners('end');
      writeable = false;
      changes = null;
    };

    changes.on('error', () => {
      writeable = false;
    });
    changes.on('end', () =>{
      writeable = false;
    });

    process.nextTick(() => {
      next(null, changes);
    });

    dispatcher.on(`dialog/message::${id}`, (data) => {
      time = data.elapsedTime || 0;
      if (writeable) {
        changes.write(data);
      }
    });
  };
  Dialog.remoteMethod('message-stream', {
    description: 'Generate new American test randomly according to selected skills',
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: {
      arg: 'changes',
      type: 'ReadableStream',
      json: true,
    },
    http: { path: '/:id/message-stream', verb: 'get' },
  },
  );
};
