let events = require('events');

// Create an eventEmitter object
let eventEmitter = new events.EventEmitter();

module.exports = eventEmitter;
