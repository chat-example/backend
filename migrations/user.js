const dummyAdmin = {
  password: 'q1w2e3r4',
  email: 'admin@dummy.com',
  username: 'admin',
};
const simpleUser = {
  password: 'q1w2e3r4',
  email: 'simple@user.com',
  username: 'simple',
};

let createUser = async (dataSource, data, roleName) => {
  const { email, username, password } = data;
  const user = await dataSource.models.user.create({
    email,
    username,
    password,
  });

  return await user.save();
};

module.exports = {
  up: async (dataSource, next) => {
    await createUser(dataSource, dummyAdmin, 'admin');
    await createUser(dataSource, simpleUser, 'user');
    next();
  },
  down: async (dataSource, next) => {
    await dataSource.models.user.destroyAll();
    next();
  },
};
